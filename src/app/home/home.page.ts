import { Component, OnInit } from '@angular/core';
import { CLUB_VALENCIA } from './mock-clubs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  ClubValencia = CLUB_VALENCIA;

  constructor() {}

  ngOnInit() {
  }

}
  
